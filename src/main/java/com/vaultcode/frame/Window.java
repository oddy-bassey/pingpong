package com.vaultcode.frame;

import com.vaultcode.comoponent.Text;
import com.vaultcode.controller.AIController;
import com.vaultcode.comoponent.Ball;
import com.vaultcode.comoponent.Rect;
import com.vaultcode.controller.PlayerController;
import com.vaultcode.listener.GameKeyListener;
import com.vaultcode.util.Constants;
import com.vaultcode.util.Time;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

public class Window extends JFrame implements Runnable {
    private Logger log  = Logger.getLogger(Window.class.getName());
    private Graphics2D graphics2D;
    private GameKeyListener keyListener;
    private Rect player, aiPlayer, ballRect;
    private PlayerController playerController;
    private AIController aiPlayerController;
    private Ball ball;
    public Text leftScoreText, rightScoreText, gameLevelText;
    private boolean isRunning = true;

    public Window() {
        this.setTitle(Constants.SCREEN_TITLE);
        this.setSize(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Constants.TOOLBAR_HEIGHT = this.getInsets().top + 9.0;
        Constants.BOTTOM_INSETS = this.getInsets().bottom + 12.0;

        leftScoreText = new Text("0", new Font("Times New Roman", Font.PLAIN, Constants.TEXT_SIZE),
                Constants.TEXT_X_POS, Constants.TEXT_Y_POS, Constants.AI_PADDLE_COLOR);
        rightScoreText = new Text("0", new Font("Times New Roman", Font.PLAIN, Constants.TEXT_SIZE),
                Constants.SCREEN_WIDTH - Constants.TEXT_X_POS - 20, Constants.TEXT_Y_POS, Constants.PLAYER_PADDLE_COLOR);
        gameLevelText = new Text("Level 1", new Font("Times New Roman", Font.PLAIN, Constants.TEXT_SIZE - 5),
                Constants.TEXT_X_POS - 20, Constants.SCREEN_HEIGHT - 22, Color.WHITE);

        this.graphics2D = (Graphics2D) this.getGraphics();
        this.keyListener = new GameKeyListener();

        this.player = new Rect(Constants.SCREEN_WIDTH - Constants.HZ_PADDING - Constants.PADDLE_WIDTH, 40,Constants.PADDLE_WIDTH, Constants.PADDLE_HEIGHT, Constants.PLAYER_PADDLE_COLOR);
        playerController = new PlayerController(player, keyListener);

        this.aiPlayer = new Rect(Constants.HZ_PADDING, 40, Constants.PADDLE_WIDTH, Constants.PADDLE_HEIGHT, Constants.AI_PADDLE_COLOR);
        this.ballRect = new Rect(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2, Constants.BALL_WIDTH, Constants.BALL_WIDTH, Constants.BALL_COLOR);
        aiPlayerController = new AIController(new PlayerController(aiPlayer), ballRect);

        this.ball = new Ball(ballRect, aiPlayer, player, log, leftScoreText, rightScoreText, gameLevelText);

        this.addKeyListener(keyListener);
    }

    public void update(double deltaTime) {
        Image doubleBufferImage = createImage(getWidth(), getHeight());
        Graphics doubleBufferGraphics = doubleBufferImage.getGraphics();
        draw(doubleBufferGraphics);

        graphics2D.drawImage(doubleBufferImage, 0, 0, this);

        playerController.update(deltaTime);
        aiPlayerController.update(deltaTime);
        ball.update(deltaTime);
    }

    public void draw(Graphics doubleBufferGraphics) {
        Graphics2D bufferGraphics2D = (Graphics2D) doubleBufferGraphics;
        bufferGraphics2D.setColor(Color.BLACK);
        bufferGraphics2D.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        bufferGraphics2D.setColor(Color.WHITE);
        bufferGraphics2D.fillRect(0, 32, Constants.SCREEN_WIDTH, 5);

        bufferGraphics2D.setColor(Color.WHITE);
        bufferGraphics2D.fillRect(0, Constants.SCREEN_HEIGHT - 14, Constants.SCREEN_WIDTH, 5);

        bufferGraphics2D.setColor(Color.WHITE);
        bufferGraphics2D.fillRect((Constants.SCREEN_WIDTH / 2) - 1, 0, 2, Constants.SCREEN_HEIGHT);

        leftScoreText.draw(bufferGraphics2D);
        rightScoreText.draw(bufferGraphics2D);
        gameLevelText.draw(bufferGraphics2D);

        player.draw(bufferGraphics2D, false);
        aiPlayer.draw(bufferGraphics2D, false);
        ballRect.draw(bufferGraphics2D, true);
    }

    public void stop() {
        isRunning = false;
    }

    @Override
    public void run() {
        double lastFrameTime = 0.0;
        while (isRunning) {
            double time = Time.getTime();
            double deltaTime = time - lastFrameTime;
            lastFrameTime = time;

            update(deltaTime);
        }
    }
}
