package com.vaultcode.frame;

import com.vaultcode.App;
import com.vaultcode.comoponent.Text;
import com.vaultcode.listener.GameKeyListener;
import com.vaultcode.listener.MouseListener;
import com.vaultcode.util.Constants;
import com.vaultcode.util.Time;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

public class MainMenu extends JFrame implements Runnable {
    private Logger log  = Logger.getLogger(MainMenu.class.getName());
    private Graphics2D graphics2D;
    private GameKeyListener keyListener;
    private MouseListener mouseListener;
    private Text startGame, exitGame, pong;
    private boolean isRunning = true;

    public MainMenu() {
        this.setTitle(Constants.SCREEN_TITLE);
        this.setSize(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.keyListener = new GameKeyListener();
        this.mouseListener = new MouseListener();
        this.graphics2D = (Graphics2D) this.getGraphics();

        this.startGame = new Text("Start Game", new Font("Times New Roman", Font.PLAIN, 40),
                Constants.SCREEN_WIDTH / 2.0 - 110.0, Constants.SCREEN_HEIGHT / 2.0, Color.WHITE);
        this.exitGame = new Text("Exit", new Font("Times New Roman", Font.PLAIN, 40),
                Constants.SCREEN_WIDTH / 2.0 - 60.0, Constants.SCREEN_HEIGHT / 2.0 + 60, Color.WHITE);

        this.pong = new Text("Pong", new Font("Times New Roman", Font.PLAIN, 100),
                Constants.SCREEN_WIDTH / 2.0 - 120.0, 200, Color.WHITE);

        this.addKeyListener(keyListener);
        this.addMouseListener(mouseListener);
        this.addMouseMotionListener(mouseListener);
    }

    public void update(double deltaTime) {
        Image doubleBufferImage = createImage(getWidth(), getHeight());
        Graphics doubleBufferGraphics = doubleBufferImage.getGraphics();
        draw(doubleBufferGraphics);
        graphics2D.drawImage(doubleBufferImage, 0, 0, this);

        isTextHoveredOn(startGame);
        isTextHoveredOn(exitGame);
    }

    private void isTextHoveredOn(Text text) {
        if(mouseListener.getMouseX() > text.getX() && mouseListener.getMouseX() < text.getX() + text.getWidth() &&
                mouseListener.getMouseY() > text.getY() - text.getHeight() / 2.0 &&
                mouseListener.getMouseY() < text.getY() + text.getHeight() / 2.0) {
            text.setColor(new Color(200, 200, 200));

            if(mouseListener.isMousePressed() && text.getText().equals("Exit")) {
                App.changeState(0);
            } else if(mouseListener.isMousePressed() && text.getText().equals("Start Game")) {
                App.changeState(1);
            }
        } else {
            text.setColor(Color.WHITE);
        }
    }

    public void draw(Graphics doubleBufferGraphics) {
        Graphics2D bufferGraphics2D = (Graphics2D) doubleBufferGraphics;
        bufferGraphics2D.setColor(Color.BLACK);
        bufferGraphics2D.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        exitGame.draw(bufferGraphics2D);
        startGame.draw(bufferGraphics2D);
        pong.draw(bufferGraphics2D);
    }

    public void stop() {
        isRunning = false;
    }

    @Override
    public void run() {
        double lastFrameTime = 0.0;
        while (isRunning) {
            double time = Time.getTime();
            double deltaTime = time - lastFrameTime;
            lastFrameTime = time;

            update(deltaTime);
        }
        this.dispose();
    }
}
