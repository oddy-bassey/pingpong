package com.vaultcode.controller;

import com.vaultcode.comoponent.Rect;
import com.vaultcode.listener.GameKeyListener;
import com.vaultcode.util.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.awt.event.KeyEvent;

@Data
@AllArgsConstructor
public class PlayerController {

    private Rect rect;
    private GameKeyListener keyListener;

    public PlayerController(Rect aiPlayer){
        this.rect = aiPlayer;
        this.keyListener = null;
    }

    public void update(double deltaTime) {
        if(keyListener != null) {
            if (keyListener.isKeyPressed(KeyEvent.VK_UP)) {
                moveUp(deltaTime);
            } else if (keyListener.isKeyPressed(KeyEvent.VK_DOWN)) {
                moveDown(deltaTime);
            }
        }
    }

    public void moveUp(double deltaTime) {
        if(rect.getY() > Constants.TOOLBAR_HEIGHT) {
            this.rect.setY(this.rect.getY() - (Constants.PADDLE_SPEED * deltaTime));
        }
    }

    public void moveDown(double deltaTime) {
        if((this.rect.getY() + Constants.PADDLE_HEIGHT + 20) < Constants.SCREEN_HEIGHT) {
            this.rect.setY(this.rect.getY() + (Constants.PADDLE_SPEED * deltaTime));
        }
    }
}
