package com.vaultcode.controller;

import com.vaultcode.comoponent.Rect;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AIController {

    private PlayerController playerController;
    private Rect ball;

    /*
     * Check if the ball is above the AI player: move up
     * else if below the AI player: move down
     */
    public void update(double deltaTime) {
        playerController.update(deltaTime);

        Rect aiPlayer = playerController.getRect();
        if(ball.getY() < aiPlayer.getY()) {
            playerController.moveUp(deltaTime);
        } else if(ball.getY() + ball.getHeight() > aiPlayer.getY() + aiPlayer.getHeight()) {
            playerController.moveDown(deltaTime);
        }
    }
}
