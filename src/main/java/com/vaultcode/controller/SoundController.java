package com.vaultcode.controller;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class SoundController {

    private Logger log  = Logger.getLogger(SoundController.class.getName());

    AudioInputStream paddleAudioInputStream;
    AudioInputStream pointsScoreAudioInputStream;
    private Clip paddleClip;
    private Clip pointsClip;
    public SoundController() {
        try {
            File paddleClickAudio = new File("src/main/java/com/vaultcode/audio/ping-pong-paddle.wav");
            File pointsScoreAudio = new File("src/main/java/com/vaultcode/audio/score-point.wav");

            paddleAudioInputStream = AudioSystem.getAudioInputStream(paddleClickAudio);
            pointsScoreAudioInputStream = AudioSystem.getAudioInputStream(pointsScoreAudio);

            paddleClip = AudioSystem.getClip();
            pointsClip = AudioSystem.getClip();

            paddleClip.open(paddleAudioInputStream);
            pointsClip.open(pointsScoreAudioInputStream);
        } catch (Exception exception) {
            log.severe(exception.getMessage());
        }
    }

    public void startPaddleClip() {
        paddleClip.start();
    }

    public void startPointsClip() {
        pointsClip.start();
    }

    public void stopPaddleClp() {
        paddleClip.stop();
        paddleClip.flush();
    }

    public void stopPointsClip() {
        pointsClip.stop();
        pointsClip.flush();
    }

    public void closeAudioStreams() {
        try {
            stopPaddleClp();
            stopPointsClip();

            paddleAudioInputStream.close();
            pointsScoreAudioInputStream.close();
        } catch (IOException exception) {
            log.info(exception.getMessage());
        }
    }
}
