package com.vaultcode.listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MouseListener extends MouseAdapter implements MouseMotionListener {

    private Boolean isPressed = false;
    private double x, y;
    @Override
    public void mousePressed(MouseEvent e) {
        isPressed = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        isPressed = false;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        x = e.getX();
        y = e.getY();
    }

    public double getMouseX() {
        return x;
    }

    public double getMouseY() {
        return y;
    }

    public boolean isMousePressed() {
        return isPressed;
    }
}
