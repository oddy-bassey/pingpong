package com.vaultcode;

import com.vaultcode.controller.SoundController;
import com.vaultcode.frame.MainMenu;
import com.vaultcode.frame.Window;

import java.util.logging.Logger;

/**
 * Hello world!
 *
 */
public class App {
    static Logger log  = Logger.getLogger(App.class.getName());
    public static int state = 1;
    public static Thread mainThread;
    private static MainMenu menu;
    private static Window window;

    public static void main( String[] args ) {
        log.info( "Hello, my first 2D game!" );

        menu = new MainMenu();
        mainThread = new Thread(menu);
        mainThread.start();
    }

    public static void changeState(int newState) {

        if(newState == 1) {
            menu.stop();
            window = new Window();
            mainThread = new Thread(window);
            mainThread.start();
        } else if(newState == 0) {
            if(window != null) {
                log.info("Closing the game window!");
                window.stop();
            }
            if(menu != null) {
                log.info("Closing the menu window!");
                menu.stop();
            }
        }
        state = newState;
    }
}
