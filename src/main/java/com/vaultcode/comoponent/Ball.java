package com.vaultcode.comoponent;

import com.vaultcode.App;
import com.vaultcode.util.Constants;
import com.vaultcode.util.SoundPlayer;
import lombok.Data;

import java.util.logging.Logger;

@Data
public class Ball {

    private Rect leftPaddle;
    private Rect rightPaddle;
    private Rect ball;
    private double yVelocity = 200.5;
    private double xVelocity = -150.5;
    private static Logger log;
    private Text leftScoreText;
    private Text rightScoreText;
    private static Text gameLevelText;

    public Ball(Rect ball, Rect leftPaddle, Rect rightPaddle, Logger log, Text leftScoreText, Text rightScoreText, Text gameLevelText) {
        this.ball = ball;
        this.leftPaddle = leftPaddle;
        this.rightPaddle = rightPaddle;
        this.log = log;
        this.leftScoreText = leftScoreText;
        this.rightScoreText = rightScoreText;
        this.gameLevelText = gameLevelText;
        SoundPlayer.init();
    }

    void resetBall(double xVelocity, double yVelocity) {
        this.ball.setX(Constants.SCREEN_WIDTH / 2.0);
        this.ball.setY(Constants.SCREEN_HEIGHT / 2.0);
        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;
    }

    double calculateBounceAngle(Rect paddle) {
        double relativeIntersectY = (paddle.getY() + (paddle.getHeight()/2)) - (this.ball.getY() + (this.ball.getHeight()/2));
        double normalIntersectY = relativeIntersectY / (paddle.getHeight()/2);
        double theta = normalIntersectY * Constants.MAX_ANGLE;

        return Math.toRadians(theta);
    }

    public void update(double deltaTime) {
        if(xVelocity < 0) {
            // check if the ball is within the left paddle width and height
            if(this.ball.getX() <= this.leftPaddle.getX() + this.leftPaddle.getWidth() && this.ball.getX() + this.ball.getWidth() >= this.leftPaddle.getX() &&
                    this.ball.getY() >= this.leftPaddle.getY() && this.ball.getY() <= this.leftPaddle.getY() + this.leftPaddle.getHeight()) {

                SoundPlayer.playPaddleClick();
                increaseBallSpeed();

                double theta = calculateBounceAngle(leftPaddle);
                double newXVelocity = Math.abs(Math.cos(theta)) * Constants.BALL_SPEED;
                double newYVelocity = (-Math.sin(theta)) * Constants.BALL_SPEED;

                double oldXVelocitySin = Math.signum(xVelocity);
                this.xVelocity = newXVelocity * (-1 * oldXVelocitySin);
                this.yVelocity = newYVelocity;
            } else if(this.ball.getX() + this.ball.getWidth() < this.leftPaddle.getX()) {

                SoundPlayer.playScorePoint();
                int rightScore = Integer.parseInt(this.rightScoreText.getText());

                if (rightScore >= Constants.WIN_SCORE) {
                    log.info("Player Wins!");
                    App.changeState(0);
                }
                rightScoreText.setText(String.valueOf(++rightScore));
                resetBall(-150.0, 10.0);
            }
        }else if(xVelocity > 0) {
            if(this.ball.getX() + this.ball.getWidth() >= this.rightPaddle.getX() && this.ball.getX() <= this.rightPaddle.getX() + this.rightPaddle.getWidth() &&
                    this.ball.getY() >= this.rightPaddle.getY() && this.ball.getY() <= this.rightPaddle.getY() + this.rightPaddle.getHeight()) {

                SoundPlayer.playPaddleClick();
                increaseBallSpeed();

                double theta = calculateBounceAngle(rightPaddle);
                double newXVelocity = Math.abs(Math.cos(theta)) * Constants.BALL_SPEED;
                double newYVelocity = (-Math.sin(theta)) * Constants.BALL_SPEED;

                double oldXVelocitySin = Math.signum(xVelocity);
                this.xVelocity = newXVelocity * (-1 * oldXVelocitySin);
                this.yVelocity = newYVelocity;
            } else if(this.ball.getX() + this.ball.getWidth() > this.rightPaddle.getX() + this.rightPaddle.getWidth()) {

                SoundPlayer.playScorePoint();
                int leftScore = Integer.parseInt(this.leftScoreText.getText());

                if (leftScore >= Constants.WIN_SCORE) {
                    log.info("Computer Wins!");
                    App.changeState(0);
                }
                leftScoreText.setText(String.valueOf(++leftScore));
                resetBall(150.0, 10.0);
            }
        }

        if(yVelocity > 0) {
            if(this.ball.getY() + this.ball.getHeight() >= Constants.SCREEN_HEIGHT - 13) {
                this.yVelocity *= -1;
            }
        } else if(yVelocity < 0) {
            if(this.ball.getY() < Constants.TOOLBAR_HEIGHT) {
                this.yVelocity *= -1;
            }
        }

        // position = position + velocity
        // velocity = velocity + acceleration
        this.ball.setX(this.ball.getX() + (this.xVelocity * deltaTime));
        this.ball.setY(this.ball.getY() + (this.yVelocity * deltaTime));
    }

    public static void increaseBallSpeed () {
        if(Constants.BPADDLE_HIT_COUNT % 5 == 0) {
            int gameLevel = (int) ((Constants.BPADDLE_HIT_COUNT / 5) + 1);

            gameLevelText.setText("Level "+ gameLevel);
            Constants.BALL_SPEED += 15;
            Constants.PADDLE_SPEED += 5;

            Constants.BPADDLE_HIT_COUNT++;
        } else {
            Constants.BPADDLE_HIT_COUNT++;
        }
    }
}
