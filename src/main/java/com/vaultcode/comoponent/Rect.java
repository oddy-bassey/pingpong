package com.vaultcode.comoponent;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.awt.*;
import java.awt.geom.Rectangle2D;

@AllArgsConstructor
@Builder
@Data
public class Rect {

    private double x, y, width, height;
    private Color color;

    public void draw(Graphics2D graphics2D, boolean isBall) {
        graphics2D.setColor(color);
        if(!isBall) {
            graphics2D.fill(new Rectangle2D.Double(x, y, width, height));
        } else {
            graphics2D.fillOval((int) x, (int) y, (int) width, (int) height);
        }
    }
}
