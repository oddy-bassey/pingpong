package com.vaultcode.util;

import java.awt.*;

public class Constants {

    public static final String SCREEN_TITLE = "Ping Pong";
    public static final Integer SCREEN_WIDTH = 800;
    public static final Integer SCREEN_HEIGHT = 600;

    public static final double PADDLE_WIDTH = 15;
    public static final double PADDLE_HEIGHT = 80;
    public static double PADDLE_SPEED = 200;
    public static double BALL_SPEED = 300;
    public static double BPADDLE_HIT_COUNT = 1;
    public static final Color AI_PADDLE_COLOR = Color.RED;
    public static final Color PLAYER_PADDLE_COLOR = Color.GREEN;
    public static final Color BALL_COLOR = Color.YELLOW;
    public static final double BALL_WIDTH = 15;
    public static final double HZ_PADDING = 20;

    public static double TOOLBAR_HEIGHT;
    public static double BOTTOM_INSETS;

    public static final double MAX_ANGLE = 45;

    public static final int TEXT_X_POS = 60;
    public static final int TEXT_Y_POS = 90;
    public static final int TEXT_SIZE = 25;
    public static final int WIN_SCORE = 11;
}
