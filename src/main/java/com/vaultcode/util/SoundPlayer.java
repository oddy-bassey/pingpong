package com.vaultcode.util;

import com.vaultcode.controller.SoundController;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SoundPlayer {
    private static SoundController sound;
    private static ExecutorService executorService;
    private static Runnable paddleClickRunner;
    private static Runnable pointsScoredRunner;

    static void createSoundController() {
        sound = new SoundController();
    }

    public static void init() {
        executorService = Executors.newFixedThreadPool(2);
        paddleClickRunner = new Runnable() {
            @Override
            public void run() {
                createSoundController();
                sound.startPaddleClip();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                closePlayer();
            }
        };

        pointsScoredRunner = new Runnable() {
            @Override
            public void run() {
                createSoundController();
                sound.startPointsClip();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                closePlayer();
            }
        };
    }

    public static void playPaddleClick() {
        executorService.execute(paddleClickRunner);
    }

    public static void playScorePoint() {
        executorService.execute(pointsScoredRunner);
    }

    public static void closePlayer() {
        if(sound != null) {
            sound.closeAudioStreams();
        }
    }
}
