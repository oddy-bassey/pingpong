# PingPong
This is a simple pingpong game written in Java. The game features concepts like:
* movement
    * player movement
    * ball displacement
* object collision and detection
* game scores
* game level (Improvement)
  * increased ball speed 
  * increased paddle speed
* audio (Improvement)
  * ball collision with paddle
  * point gained

![PingPong](/pong.png)

You can learn to write the game at this youtube channel
[GamesWithGabe PingPong tutorial](https://www.youtube.com/watch?v=-uEFy0nPlgM&list=PLtrSb4XxIVborujf7DDM2MCi6yyMCw46d)